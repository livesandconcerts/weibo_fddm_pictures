# Weibo_FDDM_Pictures

订阅微博博主(示例为订阅房东的猫相关微博)图片更新，结合幻灯片软件等实现半自动追星（每天定时抓取微博中的图片，下载到指定文件夹，回到家之后幻灯片放映该文件夹即可）。

## 简介

基于Linux环境实现，Mac用户下载几个软件包依赖应该就能直接用（未测试），Windows用户请自行探索Windows Subsystem Linux。

### 注意事项

1. 访问rsshub.app需自行调试，更换其他自建网址也可正常使用。
2. 使用前请确认已安装以下软件包依赖：
  - bc, nproc。用于自动配置多线程下载数为可用线程数-1。
  - curl。用于拉取RSS更新，可自行更换成wget。
  - grep, sort, uniq, comm, find。用于处理RSS文档，提取下载链接。这几个软件一般Linux发行版都自带。

### 配置文件指南

一共有两处可更改配置的地方，一处是在getPictures的开头config部分，用于修改和程序密切相关的配置，初次使用时需稍作改动；另一处是`config/uids.txt`，用于处理订阅信息。

## 快速上手

1. 找一个能连上互联网的Linux环境，找一个国内能直连的rsshub网址并修改getPictures文件开头config部分中的RSS_PREFIX（然后删掉PROXY并去掉curl命令里和proxy相关的部分）
2. git clone或下载该项目的压缩包到本地解压
3. 在合适的路径，为getPictures文件增加可执行权限：`chmod +x ./getPictures`
4. 下载软件包依赖，如果你使用ArchLinux，可以直接执行`yay -S bc nproc curl grep sort uniq comm find --needed`。其他Linux发行版用户请参看自己发行版的软件包管理器手册。
5. 运行`./getPictures`，烧壶开水泡杯茶，然后回来看猫猫。